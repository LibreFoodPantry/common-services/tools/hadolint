#!/usr/bin/env bash

# Modify the two lines below to direct where hadolint
# should do its checks. In the example below:
#   - SCRIPT_DIR is set to the directory where the script exists
#   - The cd command assumes the script is one level below the top level
#     of the project.

# SCRIPT_DIR="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &>\
#   /dev/null && pwd)"
# cd "$SCRIPT_DIR/.." || exit

echo -e "\nRunning hadolint\n"
docker run --rm -v "${PWD}":/app/project -w /app/project \
    registry.gitlab.com/pipeline-components/hadolint:latest \
    /bin/sh -c \
    "find . -name .git -type d -prune -o -type f  -name Dockerfile -print0 |
        xargs -0 -P $(nproc) -r -n1 hadolint"
