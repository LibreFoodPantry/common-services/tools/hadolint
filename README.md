# hadolint

[Hadolint](https://github.com/hadolint/hadolint) is a smarter Dockerfile linter
that helps you build best practice Docker images.

## Using in the Pipeline

To enable hadolint in your project's pipeline add `hadolint` to the `ENABLE_JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "hadolint"
```

The ENABLE_JOBS variable is a comma-separated string value, eg.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install [hadolint](https://marketplace.visualstudio.com/items?itemName=exiasr.hadolint)
for Visual Studio Code.

## Using Locally in Your Development Environment

hadolint can be run locally in a shell script in a linux-based bash shell.
[`hadolint.sh`](hadolint.sh) in this project is an example that you can copy
to your project or paste into your own shell script.

## Configuration

[Hadolint configuration](https://github.com/hadolint/hadolint/blob/master/README.md#configure)
should be implemented if your project includes Dockerfiles.

## Licensing

- Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
- Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
- Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
